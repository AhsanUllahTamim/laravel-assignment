<?php

namespace App\Http\Controllers;
use App\Models\Companies;
use Image;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;


use Illuminate\Http\Request;

class CompaniesController extends Controller
{

    //


    public function create(){


        return view('company.create');
    }

    public function store(Request $request){

        $companydata=$request->all();

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = time() . '.' . $file->getClientOriginalExtension();
            Image::make($request->file('image'))
                ->resize(300, 200)
                ->save(storage_path() . '/app/public/companies/' . $fileName);
                $companydata['image'] = $fileName;
        }

       
        //    dd($companydata);

        Companies::create($companydata);

        return Redirect()->route('company.index');





     }
     public function index(){

        $companydata=Companies::orderBy('id','desc')->get();

        //   dd($companydata);

        return view('company.index', compact('companydata'));


     }
     public function edit($id){

      $company = Companies::findOrFail($id);


      return view('company.edit', compact('company'));
     }

     public function show($id){
 
        //  dd($id);

         $company= Companies::where('id', $id)->firstOrFail();

        


            return view('company.show' , compact('company'));


    }
    

      public function update(Companies $company, Request $request){
         try
         {
          $companydata=request()->all();



        if ($request->hasFile('image')) {


            $file = $request->file('image');
            // change image file name
            $fileName = time() . '.' . $file->getClientOriginalExtension();
            Image::make($request->file('image'))
                ->resize(300, 200)
                ->save(storage_path() . '/app/public/companies/' . $fileName);
                $companydata['image'] = $fileName;

            // delete old image

            $company_id=$company->id;
            $company= Companies::where('id',$company_id)->first();
            $deleteImage= 'storage/companies/'.$company->image;

            
            if (file_exists($deleteImage)) {
    
                File::delete($deleteImage);
             }
        } 
         else {
            $requestData['image'] = $company->image;
        }

          $company->update($companydata);

          return Redirect()->route('company.index');
         }
         catch (QueryException $e) 
         {
           return redirect()->back()->withInput()->withErrors($e->getMessage());
         }

      }

      public function delete(Companies $company){

        $company_id=$company->id;
        $company= Companies::where('id',$company_id)->first();
        $deleteImage= 'storage/companies/'.$company->image;
        if (file_exists($deleteImage)) {
    
          File::delete($deleteImage);
       }
        $company->delete();
         return Redirect()->route('company.index');

      }

   
     

}
