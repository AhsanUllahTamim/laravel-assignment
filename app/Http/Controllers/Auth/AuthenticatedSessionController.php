<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     */
    public function create(): View
    {
        return view('auth.login');
    }

   
    public function store(LoginRequest $request): RedirectResponse
    {

    //     $logindata=$request->toArray();
    //     // dd($logindata);

    //     $email= $request->email;
    //     $password= $request->password;

    //    $useremail = DB::table('users')->where('email',$email)->value('email');
    //    $userpassword = DB::table('users')->where('email',$email)->value('password');

    // //    dd($userpassword)
    //    if($email==$useremail && $password==$userpassword)
    //    {
    //     // dd('tamim');
    //     $request->authenticate();
    //     $request->session()->regenerate();
    //     return Redirect()->route('company.index');
    //    }
    //    else
    //    {
    //     dd('email and password dont match on records');

    //    }

        $request->authenticate();

        $request->session()->regenerate();

        return Redirect()->route('company.index');
        // return redirect()->intended(RouteServiceProvider::HOME);
    }

    /**
     * Destroy an authenticated session.
     */
    public function destroy(Request $request): RedirectResponse
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
