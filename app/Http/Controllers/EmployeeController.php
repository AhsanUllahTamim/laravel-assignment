<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    //

    public function create(){


        return view('employee.create');
    }

    public function store(Request $request){

        $companydata=$request->all();


        employee::create($companydata);

        return Redirect()->route('employee.index');



     }
     public function index(){

        $employeedata=Employee::orderBy('id','desc')->get();

        //   dd($companydata);

        return view('employee.index', compact('employeedata'));


     }
     public function edit($id){

      $employee = Employee::findOrFail($id);


      return view('employee.edit', compact('employee'));
     }

     public function show($id){
 
        //  dd($id);

         $company= Employee::where('id', $id)->firstOrFail();

            return view('company.show' , compact('company'));

    }
    

      public function update(Employee $employee, Request $request){
         try
         {
          $companydata=request()->all();

        //   dd($companydata);

          $employee->update($companydata);

          return Redirect()->route('employee.index');
         }
         catch (QueryException $e) 
         {
           return redirect()->back()->withInput()->withErrors($e->getMessage());
         }

      }

      public function delete(Employee $employee){

        $employee->delete();
         return Redirect()->route('employee.index');

      }

}
