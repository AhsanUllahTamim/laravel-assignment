<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $fillable =['employee_name','email','company_id','phone'];

    public function employeeId()
    {
        return $this->belongsTo(Companies::class, 'id');
    }
}
