<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\CompaniesController;
use App\Http\Controllers\EmployeeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/', function () {
    return view("homepage");
})->name('dashboard')->middleware(['auth']);



// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});


Route::prefix('/company')->middleware(['auth'])->group(function () {
    Route::get('/create', [CompaniesController::class, 'create'])->name('company.create');
    Route::post('/store', [CompaniesController::class, 'store'])->name('company.store');
    Route::get('/index', [CompaniesController::class, 'index'])->name('company.index');
    Route::get('/show/{id}', [CompaniesController::class, 'show'])->name('company.show');
    Route::get('/edit/{company}', [CompaniesController::class, 'edit'])->name('company.edit');
    Route::patch('/update/{company}', [CompaniesController::class, 'update'])->name('company.update');
    Route::delete('/delete/{company}', [CompaniesController::class, 'delete'])->name('company.delete');

});
    

Route::prefix('/employee')->middleware(['auth'])->group(function () {
    Route::get('/create', [EmployeeController::class, 'create'])->name('employee.create');
    Route::post('/store', [EmployeeController::class, 'store'])->name('employee.store');
    Route::get('/index', [EmployeeController::class, 'index'])->name('employee.index');
    Route::get('/show/{id}', [EmployeeController::class, 'show'])->name('employee.show');
    Route::get('/edit/{employee}', [EmployeeController::class, 'edit'])->name('employee.edit');
    Route::patch('/update/{employee}', [EmployeeController::class, 'update'])->name('employee.update');
    Route::delete('/delete/{employee}', [EmployeeController::class, 'delete'])->name('employee.delete');
    
});


require __DIR__.'/auth.php';
