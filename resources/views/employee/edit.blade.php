<x-backend.layouts.master>
    <h1 class="mt-4">Employee</h1>
        <div class="card-body">

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <form action="{{ route('employee.update', ['employee' => $employee->id]) }}" method="POST" enctype="multipart/form-data">

                @csrf
                @method('PATCH')


                <div class="mb-3">
                    <label for="title" class="form-label">Employee Name</label>
                    <input name="employee_name" type="text" class="form-control" id="title" value="{{ old('employee_name', $employee->employee_name) }}">

                    @error('title')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="Rate" class="form-label">Company Id</label>
                    <input name="company_id" type="text" class="form-control" id="price" value="{{ old('employee_id', $employee->company_id) }}">
                    @error('company_id')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                


                <div class="mb-3">
                    <label for="Rate" class="form-label">Email</label>
                    <input name="email" type="text" class="form-control" id="title" value="{{ old('email', $employee->email) }}">
                    @error('rate')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="Rate" class="form-label">Phone</label>
                    <input name="phone" type="text" class="form-control" id="price" value="{{ old('phone', $employee->phone) }}">
                    @error('website')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>


                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>


    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <script>
        tinymce.init({
            selector: 'textarea#description',
            height: 500,
            menubar: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount'
            ],
            toolbar: 'undo redo | formatselect | ' +
                'bold italic backcolor | alignleft aligncenter ' +
                'alignright alignjustify | bullist numlist outdent indent | ' +
                'removeformat | help',
            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
        });
    </script>




</x-backend.layouts.master>