

<x-backend.layouts.master>
    <h1 class="mt-4">Employees</h1>
   

            <div class="card-header">
                <i class="fas fa-table me-1"></i>
                <a class="btn btn-sm btn-primary" href="{{route('employee.create')}}">Add New</a>
            </div>
        <div class="card-body">
            <div class="dataTable-wrapper dataTable-loading no-footer sortable searchable ">
                
                    </div>
                    <div class="dataTable-container">
               <table id="datatablesSimple" class="dataTable-table">
                   <thead>
                            <tr>
                                
                                <th data-sortable="" style="width: 12.6154%;"><a href="#" class="dataTable-sorter">SL</a></th>
                                <th data-sortable="" style="width: 19.0385%;"><a href="#" class="dataTable-sorter">Employee Name</a></th>
                                <th data-sortable="" style="width: 9.13462%;"><a href="#" class="dataTable-sorter">company_id</a></th>
                                <th data-sortable="" style="width: 10.5769%;"><a href="#" class="dataTable-sorter">Email</a></th>
                                <th data-sortable="" style="width: 9.13462%;"><a href="#" class="dataTable-sorter">Phone</a></th>
                                <th data-sortable="" style="width: 26.1923%;"><a href="#" class="dataTable-sorter">Action</a></th>
                            </tr>
                   </thead>
                
                   <tbody>

                    @foreach ($employeedata as $employee)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $employee->employee_name }}</td>
                        <td>{{ $employee->company_id }}</td>
                        <td>{{ $employee->email }}</td>
                        <td>{{ $employee->phone }}</td>

                        
                        <td>
              
            <a class="btn btn-warning btn-sm" href="{{ route('employee.edit', $employee->id) }}">Edit</a>    


            <form action="{{ route('employee.delete', ['employee' => $employee->id]) }}" method="POST" 
                style="display:inline">
                @csrf
                @method('delete')
                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure want to delete ?')">Delete</button>
            </form>
      
                  </td>

                    </tr>
                    @endforeach

                  </tbody>
               </table>
        </div>
       
     </div>
        </div>
    
    






</x-backend.layouts.master>