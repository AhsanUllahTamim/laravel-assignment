



<x-backend.layouts.master>

    <h1 class="mt-4">Employee</h1>
    {{-- <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
        <li class="breadcrumb-item active">companies</li>
    </ol> --}}
{{-- 
    <div class="card mb-4"> --}}
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            employee registration
            <a class="btn btn-sm btn-primary" href="{{ route('employee.index') }}">List</a>
        </div>
        <div class="card-body">

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <form action="{{route('employee.store')}}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="mb-3">
                    <label for="" class="form-label">Employee Name</label>
                    <input name="employee_name" type="text" class="form-control" id="title" value="{{ old('product_title') }}" required>

                    @error('employee_name')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input name="email" type="email" class="form-control" id="website" value="{{ old('email') }}">
                    @error('email')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="email" class="form-label">Company Id</label>
                    <input name="company_id" type="text" class="form-control" id="company_id" value="{{ old('company_id') }}">
                    @error('email')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
               

            
                <div class="mb-3">
                    <label for="website" class="form-label">Phone Number</label>
                    <input name="phone" type="text" class="form-control" id="website" value="{{ old('colour') }}">
                    @error('website')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
               


                

                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>

    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    {{-- <script>
        tinymce.init({
  selector: 'textarea#description',
  height: 500,
  menubar: false,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table paste code help wordcount'
  ],
  toolbar: 'undo redo | formatselect | ' +
  'bold italic backcolor | alignleft aligncenter ' +
  'alignright alignjustify | bullist numlist outdent indent | ' +
  'removeformat | help',
  content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
});




    </script> --}}

</x-backend.layouts.master>