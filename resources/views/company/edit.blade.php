<x-backend.layouts.master>
    <h1 class="mt-4">Company</h1>
        <div class="card-body">

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <form action="{{ route('company.update', ['company' => $company->id]) }}" method="POST" enctype="multipart/form-data">

                @csrf
                @method('PATCH')


                <div class="mb-3">
                    <label for="title" class="form-label">Company Name</label>
                    <input name="company_name" type="text" class="form-control" id="title" value="{{ old('company_name', $company->company_name) }}">

                    @error('title')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                


                <div class="mb-3">
                    <label for="Rate" class="form-label">Email</label>
                    <input name="email" type="text" class="form-control" id="title" value="{{ old('email', $company->email) }}">
                    @error('rate')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="Rate" class="form-label">Website</label>
                    <input name="website" type="text" class="form-control" id="price" value="{{ old('website', $company->website) }}">
                    @error('website')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="image" class="form-label">Image</label>
                    <input name="image" type="file" class="form-control" id="image">
                    @error('image')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>


    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <script>
        tinymce.init({
            selector: 'textarea#description',
            height: 500,
            menubar: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount'
            ],
            toolbar: 'undo redo | formatselect | ' +
                'bold italic backcolor | alignleft aligncenter ' +
                'alignright alignjustify | bullist numlist outdent indent | ' +
                'removeformat | help',
            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
        });
    </script>




</x-backend.layouts.master>