<x-backend.layouts.master>
   

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Company Details
            <a class="btn btn-sm btn-primary" href="{{ route('company.index') }}">List</a>
        </div>
        <div class="card-body">
            <h5>Company Name: {{ $company->company_name ?? 'N/A' }}</h5>
            <br>
            <h5>Email:  {{ $company->email }}</h5>
            <br>
            <h5>Webside:  {{ $company->website }}</h5>
            <br><br>
            <h5>Logo</h5>
            <img src="{{ asset('storage/companies/'. $company->image) }}" />
           
           
        </div>

    </div>
</x-backend.layouts.master>